# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-12 16:16+0000\n"
"PO-Revision-Date: 2016-03-09 17:57+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ubuntu-mate-welcome:121
msgid "Fixing incomplete install succeeded"
msgstr "Fixing incomplete install succeeded"

#: ubuntu-mate-welcome:122
msgid "Successfully fixed an incomplete install."
msgstr "Successfully fixed an incomplete install."

#: ubuntu-mate-welcome:122
msgid "Fixing the incomplete install was successful."
msgstr "Fixing the incomplete install was successful."

#: ubuntu-mate-welcome:126
msgid "Fixing incomplete install failed"
msgstr "Fixing incomplete install failed"

#: ubuntu-mate-welcome:127
msgid "Failed to fix incomplete install."
msgstr "Failed to fix incomplete install."

#: ubuntu-mate-welcome:127
msgid "Fixing the incomplete install failed."
msgstr "Fixing the incomplete install failed."

#: ubuntu-mate-welcome:134
msgid "Fixing broken dependencies succeeded"
msgstr "Fixing broken dependencies succeeded"

#: ubuntu-mate-welcome:135
msgid "Successfully fixed broken dependencies."
msgstr "Successfully fixed broken dependencies."

#: ubuntu-mate-welcome:135
msgid "Fixing the broken dependencies was successful."
msgstr "Fixing the broken dependencies was successful."

#: ubuntu-mate-welcome:139
msgid "Fixing broken dependencies failed"
msgstr "Fixing broken dependencies failed"

#: ubuntu-mate-welcome:140
msgid "Failed to fix broken dependencies."
msgstr "Failed to fix broken dependencies."

#: ubuntu-mate-welcome:140
msgid "Fixing the broken dependencies failed."
msgstr "Fixing the broken dependencies failed."

#: ubuntu-mate-welcome:193
msgid "Install"
msgstr "Install"

#: ubuntu-mate-welcome:194
msgid "Installation of "
msgstr "Installation of "

#: ubuntu-mate-welcome:195
msgid "installed."
msgstr "installed."

#: ubuntu-mate-welcome:197
msgid "Remove"
msgstr "Remove"

#: ubuntu-mate-welcome:198
msgid "Removal of "
msgstr "Removal of "

#: ubuntu-mate-welcome:199
msgid "removed."
msgstr "removed."

#: ubuntu-mate-welcome:201
msgid "Upgrade"
msgstr "Upgrade"

#: ubuntu-mate-welcome:202
msgid "Upgrade of "
msgstr "Upgrade of "

#: ubuntu-mate-welcome:203
msgid "upgraded."
msgstr "upgraded."

#: ubuntu-mate-welcome:208 ubuntu-mate-welcome:209
msgid "complete"
msgstr "complete"

#: ubuntu-mate-welcome:209
msgid "has been successfully "
msgstr "has been successfully "

#: ubuntu-mate-welcome:211 ubuntu-mate-welcome:212
msgid "cancelled"
msgstr "cancelled"

#: ubuntu-mate-welcome:212
msgid "was cancelled."
msgstr "was cancelled."

#: ubuntu-mate-welcome:214 ubuntu-mate-welcome:215
msgid "failed"
msgstr "failed"

#: ubuntu-mate-welcome:215
msgid "failed."
msgstr "failed."

#: ubuntu-mate-welcome:351
msgid "Blu-ray AACS database install succeeded"
msgstr ""

#: ubuntu-mate-welcome:352
msgid "Successfully installed the Blu-ray AACS database."
msgstr ""

#: ubuntu-mate-welcome:352
msgid "Installation of the Blu-ray AACS database was successful."
msgstr ""

#: ubuntu-mate-welcome:355
msgid "Blu-ray AACS database install failed"
msgstr ""

#: ubuntu-mate-welcome:356
msgid "Failed to install the Blu-ray AACS database."
msgstr ""

#: ubuntu-mate-welcome:356
msgid "Installation of the Blu-ray AACS database failed."
msgstr ""
