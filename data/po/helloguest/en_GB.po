#
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-03-10 10:58+0000\n"
"PO-Revision-Date: 2016-03-10 10:59+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language-Team: : LANGUAGE\n"
"Report-Msgid-Bugs-To: you@example.com\n"
"X-Generator: Poedit 1.8.4\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en_GB\n"

#: helloguest.html:14, 33
msgid "Welcome"
msgstr "Welcome"

#: helloguest.html:23
msgid "Entering the Guest Session."
msgstr "Entering the Guest Session."

#: helloguest.html:24
msgid ""
"Guest Sessions are great for allowing other people to use this computer "
"without tampering with applications, your settings or files."
msgstr ""
"Guest Sessions are great for allowing other people to use this computer "
"without tampering with applications, your settings or files."

#: helloguest.html:28
msgid "Any data left in the session will be lost when you log out!"
msgstr "Any data left in the session will be lost when you log out!"

#: helloguest.html:29
msgid ""
"Please transfer anything you'd like to keep to an external drive, a network "
"share or cloud service."
msgstr ""
"Please transfer anything you'd like to keep to an external drive, a network "
"share or cloud service."

#: helloguest.html:32
msgid "The"
msgstr "The"

#: helloguest.html:33
msgid ""
"application introduces you to the operating system, but some features that "
"require privileges will be disabled."
msgstr ""
"application introduces you to the operating system, but some features that "
"require privileges will be disabled."

#: helloguest.html:36
msgid "Thank you for using Ubuntu MATE."
msgstr "Thank you for using Ubuntu MATE."

#: helloguest.html:38
msgid "Discover"
msgstr "Discover"

#: helloguest.html:39
msgid "Dismiss"
msgstr "Dismiss"
